import axios from 'axios'
import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'

import Root from './Root'
import router from './router'
import store from './store'

import './prototypes/array'

import {polyfill} from 'smoothscroll-polyfill'

import Scroller from './components/Scroller'
import Wrapper from './components/Wrapper'
import Avatar from './components/Avatar'

Vue.component('Scroller', Scroller);
Vue.component('Wrapper', Wrapper);
Vue.component('Avatar', Avatar);

Vue.use(Vuetify);

if (!process.env.IS_WEB) Vue.use(require('vue-electron'));

Vue.http = Vue.prototype.$http = axios;
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    components: {Root},
    router,
    store,
    template: '<Root />'
}).$mount('#app');

polyfill();
